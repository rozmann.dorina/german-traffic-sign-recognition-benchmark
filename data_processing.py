import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import ndimage
from sklearn.model_selection import StratifiedShuffleSplit


def images_to_array(set_name, img_size):
    try:
        dataset = np.zeros(shape=(len(set_name), img_size, img_size, 3))
        flattened_dataset = np.zeros(shape=(len(set_name), img_size * img_size * 3))
        labels = np.zeros(shape=(len(set_name)))

        for index in range(len(set_name)):
            print(set_name["Path"].iloc[index])
            print(index)
            image_raw = plt.imread(set_name["Path"].iloc[index])
            image_raw = ndimage.zoom(image_raw,
                                     (img_size / set_name["Height"].iloc[index], img_size / set_name["Width"].iloc[index], 1))
            image = np.divide(np.subtract(image_raw, np.average(image_raw)), np.std(image_raw))
            dataset[index, :, :, :] = image
            labels[index] = set_name["ClassId"].iloc[index]
            flattened_dataset[index, :] = dataset[index, :, :, :].flatten()
        print("Finished writing the {} data to array.".format(set_name))
    except FileNotFoundError:
        print("{} not found".format(set_name["Path"].iloc[index]))

    return dataset, flattened_dataset, labels


if __name__ == "__main__":
    train_df = pd.read_csv("Train.csv")
    test_df = pd.read_csv("Test.csv")

    split = StratifiedShuffleSplit(n_splits=2, test_size=1/5, random_state=63)

    for i, j in split.split(train_df, train_df["ClassId"]):
        training_data = train_df.loc[i]
        validation_data = train_df.loc[j]

    training_data.to_csv("training_data.csv")
    validation_data.to_csv("validation_data.csv")

    validation_set_25, flattened_validation_set_25, _ = images_to_array(validation_data, 25)
    training_set_25, flattened_training_set_25, _ = images_to_array(training_data, 25)
    test_set_25, flattened_test_set_25, _ = images_to_array(test_df, 25)

    np.save("flattened_validation_set_25x25.npy", flattened_validation_set_25)
    np.save("flattened_training_set_25x25.npy", flattened_training_set_25)
    np.save("flattened_test_set_25x25.npy", flattened_test_set_25)
    np.save("validation_set_25x25.npy", validation_set_25)
    np.save("training_set_25x25.npy", training_set_25)
    np.save("test_set_25x25.npy", test_set_25)

    validation_set, flattened_validation_set, validation_labels = images_to_array(validation_data, 50)
    training_set, flattened_training_set, training_labels = images_to_array(training_data, 50)
    test_set, flattened_test_set, test_labels = images_to_array(test_df, 50)

    np.save("flattened_validation_set_50x50.npy", flattened_validation_set)
    np.save("flattened_training_set_50x50.npy", flattened_training_set)
    np.save("flattened_test_set_50x50.npy", flattened_test_set)
    np.save("validation_set_50x50.npy", validation_set)
    np.save("training_set_50x50.npy", training_set)
    np.save("test_set_50x50.npy", test_set)
    np.save("validation_labels.npy", validation_labels)
    np.save("training_labels.npy", training_labels)
    np.save("test_labels.npy", test_labels)


