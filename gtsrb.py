import pandas as pd
import numpy as np
import keras
import matplotlib.pyplot as plt
from models import ann, cnn, svc
from utils import evaluate_model
from utils import define_save_path, exponential_decay_fn, save_model_hyperparams
from models import ann
import argparse
import time


parser = argparse.ArgumentParser()

parser.add_argument("model_type", type=str, help="The type of model to be used - can be ANN, CNN or SVM.")
parser.add_argument("resolution", type=int, help="Values are 25 or 50, depending on which resolution image is to "
                                                 "be used.")
parser.add_argument("n_layers", type=int, nargs="?", help="Number of hidden layers in ANN and CNN.")
parser.add_argument("n_neurons", type=int, nargs="?", help="Number of neurons per layer in the first hidden dense "
                                                           "layer or filters in first convolutional layer.")

args = parser.parse_args()


def training(model_type, layers, neurons, resolution):
    tstamp = time.strftime("%Y%m%d%H%M%S")
    train_args = {"model_type": args.model_type,
                  "n_layers": layers,
                  "n_filters": neurons,
                  "image_resolution": resolution,
                  "timestamp": tstamp}
    save_model_hyperparams(model_type, tstamp, train_args)

    model_save_path = define_save_path(model_type, tstamp, "h5", "checkpoint")
    early_stopping = keras.callbacks.EarlyStopping(monitor="val_loss",
                                                   patience=10,
                                                   restore_best_weights=True)
    lr_scheduler = keras.callbacks.LearningRateScheduler(exponential_decay_fn)
    model_checkpoint = keras.callbacks.ModelCheckpoint(filepath=model_save_path, monitor="val_loss",
                                                       save_best_only=True,
                                                       save_weights_only=False)
    callbacks = [model_checkpoint, early_stopping, lr_scheduler]

    if model_type == "ANN":
        ann_model = ann(layers, neurons, resolution)
        history = ann_model.fit(x=training_set,
                                y=training_labels,
                                epochs=100,
                                validation_data=(validation_set, validation_labels),
                                callbacks=callbacks)

        evaluate_model(ann_model, args.model_type, test_set, test_labels, tstamp, history)

        return ann_model, history

    if model_type == "CNN":
        cnn_model = cnn(args.n_layers, args.n_neurons, args.resolution)

        history = cnn_model.fit(x=training_set,
                                y=training_labels,
                                epochs=100,
                                validation_data=(validation_set, validation_labels),
                                callbacks=callbacks)

        evaluate_model(cnn_model, args.model_type, test_set, test_labels, tstamp, history)

        return cnn_model, history


if __name__ == "__main__":
    train_df = pd.read_csv("Train.csv")
    test_df = pd.read_csv("Test.csv")
    training_data = pd.read_csv("training_data.csv")
    validation_data = pd.read_csv("validation_data.csv")

    if args.model_type == "ANN" or args.model_type == "CNN":
        if args.resolution == 25:
            training_set = np.load("training_set_25x25.npy")
            validation_set = np.load("validation_set_25x25.npy")
            test_set = np.load("test_set_25x25.npy")
        if args.resolution == 50:
            training_set = np.load("training_set_50x50.npy")
            validation_set = np.load("validation_set_50x50.npy")
            test_set = np.load("test_set_50x50.npy")
    if args.model_type == "SVM":
        if args.resolution == 50:
            training_set = np.load("flattened_training_set_50x50.npy")
            validation_set = np.load("flattened_validation_set_50x50.npy")
            test_set = np.load("flattened_test_set_50x50.npy")
        if args.resolution == 25:
            training_set = np.load("flattened_training_set_25x25.npy")
            validation_set = np.load("flattened_validation_set_25x25.npy")
            test_set = np.load("flattened_test_set_25x25.npy")

    validation_labels = np.load("validation_labels.npy")
    training_labels = np.load("training_labels.npy")
    test_labels = np.load("test_labels.npy")

    class_counts = pd.DataFrame()
    class_counts["Counts"] = train_df["ClassId"].value_counts()
    class_counts["Counts"].sort_index().plot(kind='bar', figsize=(10, 5), title="Distribution of classes")
    plt.show()
    training_data["ClassId"].value_counts().sort_index().plot(kind='bar',
                                                              figsize=(10, 5),
                                                              title="Distribution of classes in the training set")
    plt.show()
    validation_data["ClassId"].value_counts().sort_index().plot(kind='bar',
                                                                figsize=(10, 5),
                                                                title="Distribution of classes in the validation set")
    plt.show()

    if args.model_type == "ANN":
        ANN_model, hist = training(args.model_type, args.n_layers, args.n_neurons, args.resolution)

    if args.model_type == "CNN":
        CNN_model, hist = training(args.model_type, args.n_layers, args.n_neurons, args.resolution)

    if args.model_type == "SVM":
        svm = svc()
        svm.fit(training_set, training_labels)
        val_pred = svm.predict(validation_set)
        test_pred = svm.predit(test_set)

