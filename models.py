import keras
from sklearn import svm
from sklearn.model_selection import GridSearchCV


def ann(n_layers, n_neurons, resolution):
    ann_model = keras.models.Sequential()
    ann_model.add(keras.layers.Flatten(input_shape=[resolution, resolution, 3]))
    for layer in range(n_layers):
        ann_model.add(keras.layers.Dense(n_neurons))
        ann_model.add(keras.layers.BatchNormalization())
        ann_model.add(keras.layers.Activation("relu"))
        n_neurons = n_neurons/2
    ann_model.add(keras.layers.Dense(43, activation="softmax"))

    ann_model.compile(optimizer=keras.optimizers.Adam(),
                      loss="sparse_categorical_crossentropy",
                      metrics=["accuracy"])

    return ann_model


def cnn(n_layers, n_neurons, resolution):
    cnn_model = keras.models.Sequential()
    for layer in range(n_layers):
        if layer == 0:
            cnn_model.add(keras.layers.Conv2D(n_neurons,
                                              kernel_size=(5, 5),
                                              strides=(2, 2),
                                              input_shape=(resolution, resolution, 3)))
        else:
            cnn_model.add(keras.layers.Conv2D(n_neurons,
                                              kernel_size=(3, 3),
                                              strides=(1, 1),
                                              input_shape=(resolution, resolution, 3)))
        cnn_model.add(keras.layers.BatchNormalization())
        cnn_model.add(keras.layers.Activation("relu"))
        n_neurons *= 2
    cnn_model.add(keras.layers.GlobalAveragePooling2D())
    cnn_model.add(keras.layers.Dense(100, activation="relu"))
    cnn_model.add(keras.layers.Dense(43, activation="softmax"))

    cnn_model.compile(optimizer=keras.optimizers.Adam(),
                      loss="sparse_categorical_crossentropy",
                      metrics=["accuracy"])

    return cnn_model


def svc():
    param_grid = {'C': [0.1, 1, 10], 'gamma': [0.0001, 0.001, 0.1], 'kernel': ['rbf', 'poly']}
    svc_model = svm.SVC(probability=True, verbose=True)
    model = GridSearchCV(svc_model, param_grid)

    return model


