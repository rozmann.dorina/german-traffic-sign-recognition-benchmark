import os
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import pickle


def exponential_decay_fn(epoch):
    return 0.01 * 0.1**(epoch/10)


def define_save_path(model_type, tstamp, file_format, text):
    if not os.path.exists("{}/{}".format(model_type, tstamp)):
        os.makedirs("{}/{}".format(model_type, tstamp))
    save_path = os.path.join("{}/{}/".format(model_type, tstamp), "{}_{}_{}.{}".format(model_type, tstamp, text, file_format))

    return save_path


def save_training_history(model, tstamp, hist):
    results_save_path = define_save_path(model, tstamp, "pickle", "history")
    hist_dict = hist.history
    hist_dict.update(hist.params)
    hist_dict["epoch_list"] = hist.epoch

    with open(results_save_path, 'wb') as f:
        pickle.dump(hist_dict, f)

    return None


def save_model_hyperparams(model, tstamp, dict):
    params_save_path = define_save_path(model, tstamp, "pickle", "hyperparams")
    with open(params_save_path, 'wb') as f:
        pickle.dump(dict, f)

    return None


def evaluate_model(model, model_type, test_set, test_labels, tstamp, hist):
    csv_to_save = pd.DataFrame()
    csv_to_save["labels"] = test_labels
    test_probability_matrix = model.predict(test_set)
    test_predictions = np.argmax(test_probability_matrix, axis=1)
    csv_to_save["predictions"] = test_predictions
    path = define_save_path(model_type, tstamp, "csv", "predictions")
    csv_to_save.to_csv(path)
    test_accuracy = (np.divide(np.sum(test_predictions == test_labels), len(test_predictions)))
    print("The accuracy of this {} is {}.".format(str(model_type), str(test_accuracy)))
    loss_dict = {"train_loss": hist.history["loss"],
                 "valid_loss": hist.history["val_loss"]}
    pd.DataFrame(loss_dict).plot(figsize=(8, 5))
    plt.grid(True)
    plt.gca().set_xticks(hist.epoch)
    plt.title("{} ({}) Mean squared error loss".format(model_type, tstamp))
    plt.savefig('{1}/{0}/{1}_{0}_mse_loss.svg'.format(tstamp, model_type))
    plt.show()
    save_training_history(model_type, tstamp, hist)

    return None
